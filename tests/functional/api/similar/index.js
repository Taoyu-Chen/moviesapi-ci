import chai from "chai";
import request from "supertest";

const expect = chai.expect;

let api;

const similarMovieId = 424;
const currentMovieId = 181808;
const updateMovieTitle = {
  "title": "Updated Star Wars: The Last Jedi"
};
const updatedMovieTitle = "Updated Star Wars: The Last Jedi";

let movieId;
const movie = {
  backdrop_path: "/5Iw7zQTHVRBOYpA0V6z0yypOPZh.jpg",
  genres: [
    {
      id: 14,
      name: "Fantasy"
    },
    {
      id: 12,
      name: "Adventure"
    },
    {
      id: 878,
      name: "Science Fiction"
    },
    {
      id: 28,
      name: "Action"
    }
  ],        id: 181808,
  original_language: "en",
  original_title: "Star Wars: The Last Jedi",
  overview:
    "Rey develops her newly discovered abilities with the guidance of Luke Skywalker, who is unsettled by the strength of her powers. Meanwhile, the Resistance prepares to do battle with the First Order.",
  popularity: 44.208,
  poster_path: "/kOVEVeg59E0wsnXmF9nrh6OmWII.jpg",
  release_date: "2017-12-13",
  tagline: "Darkness rises... and light to meet it",
  title: "Star Wars: The Last Jedi",
  video: false,
  vote_average: 7,
  vote_count: 9692
};

describe("Movies endpoint", () => {
  beforeEach(async () => {
    try {
      api = require("../../../../index");
    } catch (err) {
      console.error(`failed to Load user Data: ${err}`);
    }
  });
  afterEach(() => {
    api.close(); // Release PORT 8080
    delete require.cache[require.resolve("../../../../index")];
  });
  describe("GET /similar/:id ", () => {
    it("should return 20 movies and a status 200", (done) => {
      request(api)
        .get(`/api/similar/${similarMovieId}`)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.be.a("array");
          expect(res.body.length).to.equal(20);
          const result = res.body.map((movie) => movie.title);
          expect(result).to.have.members([
            "The Zookeeper's Wife",
            "The Pianist",
            "Fury",
            "Downfall",
            "The Devil's Arithmetic",
            "The Courageous Heart of Irena Sendler",
            "The Diary of Anne Frank",
            "The Photographer of Mauthausen",
            "The Eagle Has Landed",
            "Sophie's Choice",
            "The Counterfeiters",
            "Where Eagles Dare",
            "Anthropoid",
            "The Boy in the Striped Pyjamas",
            "Inglourious Basterds",
            "Suite Française",
            "Darkest Hour",
            "Allied",
            "Unbroken",
            "A Hidden Life"
          ]);
          done();
        });
    });
  });

  describe("POST /similar/", () => {
    it("should return a 201 status and the newly added movie", () => {
        return request(api)
          .post("/api/similar")
          .send(movie)
          .expect(201)
          .then((res) => {
            expect(res.body.title).equals(movie.title);
            movieId = res.body.id;
          });
      });
      after(() => {
        return request(api)
          .get(`/api/similar/movie/${movieId}`)
          .expect(200)
          .then((res) => {
            expect(res.body).to.have.property("title", movie.title);
          });
      });
  });

  describe("GET /similar/:id", () => {
    describe("when the id is valid", () => {
      it("should return the matching movie", () => {
        return request(api)
          .get(`/api/similar/movie/${movie.id}`)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            expect(res.body).to.have.property("title", movie.title);
          });
      });
    });
    describe("when the id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get("/api/similar/9999")
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect({
            message: "Unable to find movie with id: 9999.",
            status: 404,
          });
      });
    });
  });

  describe("PUT /similar/:id", () => {
      describe("when the id is valid", () => {
        it("should return with a copy of the updated movie", () => {
          return request(api)
            .put(`/api/similar/${currentMovieId}`)
            .send(updateMovieTitle)
            .expect(200)
            .then((res) => {
              expect(res.body).to.have.property("title", updatedMovieTitle);
            });
        });
        after(() => {
        return request(api)
          .get(`/api/similar/movie/${currentMovieId}`)
          .expect(200)
          .then((res) => {
            expect(res.body).to.have.property("title", updatedMovieTitle);
          });
        });
      });
      describe("when the id is invalid", () => {
        it("should return the NOT found message", () => {
          return request(api)
            .put("/api/toprated/9999")
            .send(updateMovieTitle)
            .expect({
              message: "Unable to find Movie",
              status: 404,
            });
        });
      });
    });
  describe("Delete /similar/:id", () => {
      describe("when the id is valid", () => {
        it("should return a 200 status and confirmation message", () => {
          return request(api)
            .delete(`/api/similar/${currentMovieId}`)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .expect({
              message: `Deleted movie id: ${currentMovieId}.`,
              status: 200,
            });
        });
        after(() => {
          return request(api)
            .get(`/api/toprated/${currentMovieId}`)
            .expect(404)
            .expect({
              message: `Unable to find movie with id: ${currentMovieId}.`,
              status: 404,
            });
        });
      });
      describe("when the id is invalid", () => {
          it("should return the NOT found message", () => {
          return request(api)
            .delete("/api/toprated/9999")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect({
              message: "Unable to find movie with id: 9999.",
              status: 404,
            });
        });
      });
    });
});
